BJetCalibrationTool
=====

Overview
-----

This package provides b-jet momentum corrections, to be applied after calibration with JES and selection with a b-tag discriminant.

Two corrections are possible:
- Muon-in jet correction -- muons satisfying some quality criteria are matched to the b-jets with a shrinking delta R cone. If at least one muon meets the matching criteria, the four-momentum of the highest pT muon is added to the jet four-momentum. The estimated calorimeter energy loss is removed from the muon four-momentum, under the assumption that it is reconstructed as part of the jet energy.
- Reconstructed pT ('PtReco') correction -- after matching muons, a scale factor can be applied to correct for unreconstructed or invisible momentum not accounted for by the jet reconstruction. This is derived from Higgs boson MC and tuned to improve the H(bb) mass resolution, but may not be optimal for other processes. Different (hadronic vs semi-leptonic decay) corrections are applied depending on whether any muons were matched to the b-jet.

Usage instructions
-----

### Obtaining the package

You can use `git clone` to get the package from the url (suggestion: click the `Clone` button on the gitlab page to get the full URL including authentication). For the benefit of CMake it may be better to clone into a directory with the name `BJetCalibrationTool`. Example command with http (SSH or KRB5 recommended but more verbose):
```
git clone https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/r3hh/b-jet-energy-corrections.git BJetCalibrationTool
```

It may be more convenient for you to add this package as a submodule of your analysis code repository. To match the authentication of the main clone, it is easiest to do so via relative paths, so assuming your code is at `gitlab.cern.ch/analysis_group/analysis_project`, do:
```
git submodule add -- "../../atlas-physics/HDBS/DiHiggs/r3hh/b-jet-energy-corrections.git" BJetCalibrationTool
```
If your directory hierarchy is deeper, you would need to add `../` as needed to reach the top level of `gitlab.cern.ch`.

In future you should then clone the analysis repo with `git clone --recursive` in order to get all submodules.

### Using the code

The `BJetCalibrationTool` class is recommended to be executed via one of two algorithms provided in this package. 
However, it is also possible to configure and run the tool directly from your own analysis algorithm if those cannot be incorporated.
The tool itself will carry out the following operations:
1. Modify the jet four-momentum with the two corrections above.
2. Decorate the jet with the number of matched muons ("n_muons" `int`)
3. Decorate the jet with the scale factor used to correct the reconstructed four-momentum ("PtReco_SF" `float`)

#### Basic dual-use algorithm

The [BJetCalibrationTool/BJetCalibrationAlg.h](./BJetCalibrationTool/BJetCalibrationAlg.h) can be added to your AthAnalysis or EventLoop job sequence as any other algorithm (see [tutorial](https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/alg_basic_config/) for basic configuration instructions).

You should set the following properties specifically to match your job:
- (Algorithm property) `jetsIn`: The name of the input calibrated jet collection
- (Algorithm property) `btagSelDecor`: The name of a decoration indicating whether a jet passes the b-jet selection
- (Algorithm property) `jetsOut`: The name of the corrected jet collection to be recorded to the store
- (Tool property) `muonName`: The input calibrated muon collection

A [configuration helper](./python/BJetPtCorrectionConfig.py#L5-33) using the Athena `ComponentAccumulator` is provided setting up the algorithm directly with default options, but also allowing customisation of the underlying options.
See [this tutorial](https://atlassoftwaredocs.web.cern.ch/guides/ca_configuration/ca/) for usage of `ComponentAccumulator`.
If you configure your job in this way, for the default tool configuration you can simply call:
```python
# Assumes your top level ComponentAccumulator is called `cfg'
# and you have imported AthenaConfiguration AllConfigFlags.ConfigFlags
from BJetCalibrationTool.BJetPtCorrectionConfig import BJetCalibSystAlgCfg
cfg.merge(
    BJetCalibSystAlgCfg(
        ConfigFlags,
        jetName="AnalysisJets",
        muonName="AnalysisMuons",
        # Example decoration name indicating
        # jet was b-tagged
        btagSelDecor="pass_bdl1d_77pc",
        calibSuffix='bjetcalib'
    )
)
```

#### Systematics-aware algorithm

The systematics-aware version of the algorithm is similar to the basic one, but extended to integrate systematic uncertainty loops as supported by the CP Alg approach.
See the [CP alg tutorial](https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/cpalg_basic_sequence/) for guidance on configuring CP alg sequences.

The configuration helper provided allows you to extend the jet + flavour-tagging CP alg sequences with the b-jet pt correction alg as follows:
```python
# Set up muon calibration sequence
muonSequence = makeMuonAnalysisSequence(
    ... # See docs for args
)
# Typical names, modify as necessary
muonSequence.configure(
    inputName="Muons",
    outputName="AnalysisMuons",
)

# Set up jet calibration sequence
jetSequence = makeJetAnalysisSequence(
    ... # See docs for args
)
# Append b-tagging CP algorithms to jet sequence
makeFTagAnalysisSequence(
    jetSequence,
    btagger="DL1dv00",
    btagWP="FixedCutBEff_77",
    ... # See docs for other args
)
# Append b-jet pt corrections to jet sequence
makeBJetCalibAnalysisSequence(
            flags=ConfigFlags, # if not in CA job, you can set flags=None
            jetSequence,
            muonName="AnalysisMuons",
            # Set by FTag sequence
                btagSelDecor='ftag_select_DL1dv00_FixedCutBEff_77',
        )
jetSequence.configure(
        inputName="AntiKt4EMPFlowJets",
        outputName="AnalysisAntiKt4EMPFlowJets",
    )
```
*Note: The `flags` argument is provided for consistency with `ComponentAccumulator` configuration functions, but currently is unused. So non-CA jobs can set `flags=None` to satisfy the function signature.*

#### Direct tool setup

To use the tool directly, the following steps are required:
1. Add a `ToolHandle<IBJetCalibrationTool>` to your analysis code. Note that this `ToolHandle` takes an `IBJetCalibrationTool` interface class as the input, which supports future development of other concrete implementations.
2. In the algorithm `initialize()` function (or the equivalent called at the start of the job), call `ToolHandle::retrieve()` to configure and validate the tool setup.
3. In the algorithm `execute()` function (or the equivalent called on every event), retrieve the relevant jet and muon containers. We do this to avoid needing to rerun the muon selection when correcting each jet.
4. Call the `selectMuonsForCorrection(...)` function from the `ToolHandle` to retrieve a `std::vector<const xAOD::Muon*>` containing the selected muons to be passed to the b-jet correction function.
5. Make a shallow copy of the jet container, so that it can be modified. *If you have a non-const `JetContainer` from upstream in your code where you apply calibrations, you can use this instead.*
6. Initialise the `n_muons` and `PtReco_SF` decorations on the jet collection to `0` and `1.0` respectively. *We need to avoid conditionally decorating the jets, as decorations that appear only in some events can cause problems in reading the data later.*
7. Looping over selected b-jets, pass the jet together with the selected muon vector to the `applyBJetCalibration(...)` function.
8. If necessary, once the loop is complete, record the jets to the event store.
9. Add the appropriate configuration to your job steering script, including configuring the `MuonSelectionTool` used by the `BJetCalibrationTool`. The default options should generally suffice, but see also the [provided configuration helper](./python/BJetPtCorrectionConfig.py#L5-33) for details.

See this [ToolHandle tutorial](https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/basic_ana_tool_handle/) for generic instructions on how to add tools to your C++ code and configure them in python.
The [BJetCalibrationTool/BJetCalibrationAlg.h](./BJetCalibrationTool/BJetCalibrationAlg.h) class in this package also serves as a simple example if it cannot be executed directly in your code.

Original package location: https://gitlab.cern.ch/mobelfki/BJetCalibrationTool
Original TWiki instructions: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BJetCorrectionsHowTo
    
