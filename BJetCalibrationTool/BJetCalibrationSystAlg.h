/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author TJ Khoo


#ifndef BJetCalibrationSystAlg_H
#define BJetCalibrationSystAlg_H

#include "AnaAlgorithm/AnaAlgorithm.h"
#include <SystematicsHandles/SysCopyHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "BJetCalibrationTool/IBJetCalibrationTool.h"

/// \brief an algorithm for calling \ref IBJetCalibrationTool

class BJetCalibrationSystAlg final : public EL::AnaAlgorithm
{
    /// \brief the standard constructor
    public:
    BJetCalibrationSystAlg (const std::string& name, 
                        ISvcLocator* pSvcLocator);

    public:
    StatusCode initialize () override;

    public:
    StatusCode execute () override;

    /// \brief the calibration tool
    private:
    ToolHandle<IBJetCalibrationTool> m_calibrationTool{
        this, "BJetCalibTool", "", "Tool implementing the calibration",
    };

    /// \brief configuration for retrieval, copy and output of jets
    private:
    CP::SysListHandle m_systematicsList {this};
    CP::SysCopyHandle<xAOD::JetContainer> m_jetHandle {
        this, "jets", "AnalysisJets", "the jet collection to run on"};

    CP::SysReadHandle<xAOD::MuonContainer> m_muonHandle{
        this, "muons", "AnalysisMuons", "Muon collection to retrieve"};

    /// \brief decoration name for b-jet preselection
    private:
    Gaudi::Property<std::string> m_btagSelDecor {
        this, "btagSelDecor", "", "decoration holding b-jet pass info"};
    SG::AuxElement::ConstAccessor<char> m_acc_btagSel{""};
};
#endif

