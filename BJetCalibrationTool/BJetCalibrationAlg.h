/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author TJ Khoo


#ifndef BJetCalibrationAlg_H
#define BJetCalibrationAlg_H

#include "AnaAlgorithm/AnaAlgorithm.h"
#include "AsgDataHandles/ReadHandleKey.h"
#include "AsgDataHandles/WriteHandleKey.h"
#include "xAODJet/JetContainer.h"
#include "BJetCalibrationTool/IBJetCalibrationTool.h"

/// \brief an algorithm for calling \ref IBJetCalibrationTool

class BJetCalibrationAlg final : public EL::AnaAlgorithm
{
    /// \brief the standard constructor
    public:
    BJetCalibrationAlg (const std::string& name, 
                        ISvcLocator* pSvcLocator);

    public:
    StatusCode initialize () override;

    public:
    StatusCode execute () override;

    /// \brief the calibration tool
    private:
    ToolHandle<IBJetCalibrationTool> m_calibrationTool{
        this, "BJetCalibTool", "", "Tool implementing the calibration",
    };

    /// \brief the jet collection we run on
    private:
    // Maybe change to systematics handle
    SG::ReadHandleKey<xAOD::JetContainer> m_jetsInKey {
        this, "jetsIn", "", "the jet collection to run on"};
    SG::WriteHandleKey<xAOD::JetContainer> m_jetsOutKey {
        this, "jetsOut", "", "the jet collection to write out"};

    SG::ReadHandleKey<xAOD::MuonContainer> m_muonsKey{this, "muonsIn", "Muons", "Muon collection to retrieve"};

    /// \brief decoration name for b-jet preselection
    private:
    Gaudi::Property<std::string> m_btagSelDecor {
        this, "btagSelDecor", "", "decoration holding b-jet pass info"};
    SG::AuxElement::ConstAccessor<char> m_acc_btagSel{""};
};
#endif

