///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/

// BJetCalibrationTool.cxx
// Source file for class BJetCalibrationTool
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
///////////////////////////////////////////////////////////////////

#ifndef BJetCalibrationTool_APPLYBJETCALIBRATION_H
#define BJetCalibrationTool_APPLYBJETCALIBRATION_H 1

#include <string.h>
#include <memory>

#include <TString.h>
#include <TEnv.h>

#include "AsgTools/AsgTool.h"
#include "AsgTools/ToolHandle.h"
#include "BJetCalibrationTool/IBJetCalibrationTool.h"

// BJetCalibrationTool includes
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "MuonSelectorTools/MuonSelectionTool.h"

class BJetCalibrationTool : public virtual IBJetCalibrationTool, public asg::AsgTool {

	ASG_TOOL_CLASS1(BJetCalibrationTool, IBJetCalibrationTool)

public:

  /// Constructor with parameter name: 
  BJetCalibrationTool(const std::string& name = "BJetCalibrationTool");

  /// Destructor: 
  virtual ~BJetCalibrationTool() = default;
  virtual StatusCode initializeTool(const std::string& name);
  StatusCode initialize();

  virtual std::vector<const xAOD::Muon*> selectMuonsForCorrection(const xAOD::MuonContainer& muonsIn);
  virtual StatusCode applyBJetCalibration(xAOD::Jet& jet, const std::vector<const xAOD::Muon*>& muons_for_correction);
protected:
  /// This is where the actual calibration code goes.
 

//Private methods
private:
  
  std::vector< const xAOD::Muon* > getMuonInJet(xAOD::Jet& jet, const std::vector< const xAOD::Muon* >& muons);
  void addMuon(xAOD::Jet& jet, const std::vector< const xAOD::Muon* >& muons);
  const xAOD::Muon* getMuon(const std::vector< const xAOD::Muon* >& muons);
  void applyPtReco(xAOD::Jet& jet);

//Private members
private:

  //Variables for configuration
  Gaudi::Property<std::string> m_JetAlgo{this, "JetCollection", "AntiKt4EMPFlow", "Jet alg for retrieving calib file"}; 
  
  Gaudi::Property<double>      m_Jet_Muon_dR{this, "Jet_Muon_dR", 0.4, "Muon-matching dR cut"};
  Gaudi::Property<bool>        m_doVR{this, "doVR", true, "Use variable radius for muon matching"};
  Gaudi::Property<bool> 	     m_doPtReco{this, "doPtReco", true, "Apply reconstructed pt correction"};
  
  Gaudi::Property<std::string> m_PtReco{this, "PtRecoFile", "PtReco_Correction_MV2c10_DL1r_05032020.root", "ROOT file for reconstructed pt correction"};
  Gaudi::Property<std::string> m_BJet_Tagger{this, "BJet_Tagger", "DL1r", "B-tagger used for deriving correction"};
 
  ToolHandle<CP::MuonSelectionTool> m_muonSelection{this, "MuonSelectionTool", "", "Tool for muon quality selection"};
  
  // Clean up automatically
  std::unique_ptr<TFile> m_PtRecoFile;
  // Memory is managed by the file
  TH1F  *m_Semi_Histo;
  TH1F  *m_Had_Histo;
 
 
}; 

#endif //> !BJetCalibrationTool_APPLYBJETCALIBRATION_H
