/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author TJ Khoo


//
// includes
//

#include <memory>
#include "BJetCalibrationTool/BJetCalibrationAlg.h"
#include "AsgDataHandles/ReadHandle.h"
#include "AsgDataHandles/WriteHandle.h"
#include "xAODCore/ShallowCopy.h"

//
// method implementations
//

BJetCalibrationAlg ::
BJetCalibrationAlg (const std::string& name, 
                    ISvcLocator* pSvcLocator)
: AnaAlgorithm (name, pSvcLocator)
{
}



StatusCode BJetCalibrationAlg ::
initialize ()
{
    ANA_CHECK (m_calibrationTool.retrieve());
    ANA_CHECK (m_jetsInKey.initialize ());
    ANA_CHECK (m_jetsOutKey.initialize ());
    ANA_CHECK (m_muonsKey.initialize ());

    m_acc_btagSel = SG::AuxElement::ConstAccessor<char>(m_btagSelDecor);

    return StatusCode::SUCCESS;
}



StatusCode BJetCalibrationAlg ::
execute ()
{

    SG::ReadHandle<xAOD::JetContainer> jetsIn(m_jetsInKey);
    ANA_CHECK (jetsIn.isValid());

    // If this is a view-container, get the owning container
    // as this is what we need to copy
    // Some solution needed for reproducing selection...
    const xAOD::JetContainer* jetsToCopy = jetsIn.get();
    if (jetsToCopy->ownPolicy()==SG::VIEW_ELEMENTS && jetsToCopy->size()>0) {
        jetsToCopy = static_cast<const xAOD::JetContainer*>((*jetsToCopy)[0]->container());
    }
    std::pair<xAOD::JetContainer*,xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*jetsToCopy);

    // Retrieve muons for correction just once
	SG::ReadHandle<xAOD::MuonContainer> muons(m_muonsKey);
	ANA_CHECK( muons.isValid() );
    std::vector<const xAOD::Muon*> muons_for_correction = m_calibrationTool->selectMuonsForCorrection(*muons);

    // Initialise decorations with defaults to avoid conditional decoration bugs
    const static SG::AuxElement::Decorator<float> dec_PtRecoF("PtReco_SF");
	const static SG::AuxElement::Decorator<int> dec_NMu("n_muons");

    // Make sure that memory is managed safely
    std::unique_ptr<xAOD::JetContainer> jetsOut(shallowcopy.first);
    std::unique_ptr<xAOD::ShallowAuxContainer> jetsAux(shallowcopy.second);
    for (xAOD::Jet *jet : *jetsOut)
    {
        dec_PtRecoF(*jet) = 1.;
        dec_NMu(*jet) = 0;

        jet->setJetP4("NoBJetCalibMomentum",jet->jetP4());
        if(m_acc_btagSel(*jet)) {
            ANA_CHECK(m_calibrationTool->applyBJetCalibration(*jet,muons_for_correction));
        }
    }

    SG::WriteHandle<xAOD::JetContainer> jetsOutHandle(m_jetsOutKey);
    ATH_CHECK( jetsOutHandle.record(std::move(jetsOut), std::move(jetsAux)) );

    return StatusCode::SUCCESS;
}
