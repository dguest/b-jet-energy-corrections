/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author TJ Khoo


//
// includes
//

#include <memory>
#include "BJetCalibrationTool/BJetCalibrationSystAlg.h"

//
// method implementations
//

BJetCalibrationSystAlg ::
BJetCalibrationSystAlg (const std::string& name, 
                    ISvcLocator* pSvcLocator)
: AnaAlgorithm (name, pSvcLocator)
{
}



StatusCode BJetCalibrationSystAlg ::
initialize ()
{
    ANA_CHECK (m_calibrationTool.retrieve());
    ANA_CHECK (m_jetHandle.initialize (m_systematicsList));
    ANA_CHECK (m_muonHandle.initialize (m_systematicsList));
    ANA_CHECK (m_systematicsList.initialize());

    m_acc_btagSel = SG::AuxElement::ConstAccessor<char>(m_btagSelDecor);

    return StatusCode::SUCCESS;
}



StatusCode BJetCalibrationSystAlg ::
execute ()
{

    // Initialise decorations with defaults to avoid conditional decoration bugs
    const static SG::AuxElement::Decorator<float> dec_PtRecoF("PtReco_SF");
	const static SG::AuxElement::Decorator<int> dec_NMu("n_muons");

    for (const auto& sys : m_systematicsList.systematicsVector())
    {

        // Retrieve muons for correction just once
        const xAOD::MuonContainer *muons = nullptr;
        ANA_CHECK (m_muonHandle.retrieve (muons, sys));
        std::vector<const xAOD::Muon*> muons_for_correction = m_calibrationTool->selectMuonsForCorrection(*muons);
        ATH_MSG_VERBOSE("Selected " << muons_for_correction.size() << " muons for b-jet calibration");

        xAOD::JetContainer *jets = nullptr;
        ANA_CHECK (m_jetHandle.getCopy (jets, sys));
        ATH_MSG_VERBOSE("Jets to copy size: " << jets->size());
        for(xAOD::Jet* jet : *jets) {
            dec_PtRecoF(*jet) = 1.;
            dec_NMu(*jet) = 0;
            jet->setJetP4("NoBJetCalibMomentum",jet->jetP4());

            if(m_acc_btagSel(*jet)) {
                ATH_MSG_VERBOSE("B-Jet pt before calibration: " << jet->pt());
                ANA_CHECK(m_calibrationTool->applyBJetCalibration(*jet,muons_for_correction));
                ATH_MSG_VERBOSE("B-Jet pt after calibration: " << jet->pt());
            }
        }
    }

    return StatusCode::SUCCESS;
}
